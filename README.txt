CiviCRM allows administrators to create profiles for "public" (i.e. anonymous)
users, but does not allow you to limit certain profiles to authenticated users?
This module allows you to assign permissions by role to public profiles.

Author: jason@dharmatech.org
http://dharmatech.org/
